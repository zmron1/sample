package com.example.samplelibrary

import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class Sample : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

    }

    private fun revealToast() {
        Toast.makeText(this as Sample, "Test Toast", Toast.LENGTH_LONG)
    }
}